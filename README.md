Tournament.py:

Display a GUI for a tournamant of different two person games where points are awarded for winning games, drawing games, playing new games and playing against new opponents. A leader board can also be displayed.

Input:

1. Games.txt   - A list of the games that be played
2. Players.txt - A list of the contestants
3. scoring.txt - The score for each outcome, new game and opponent

Output:

1. log.txt     - Ongoing log of each scoring event
2. results.txt - The scores etc. for each player