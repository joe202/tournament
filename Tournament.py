#!python
#===============================================================================
# File: Tournament.py
# Display a GUI for a tournamant of different two person games where points are
# awarded for winning games, drawing games, playing new games and against new
# opponents. A leader board can also be displayed.
#
# Input:  Games.txt   - A list of the games that be played
#         Players.txt - A list of the contestants
#         scoring.txt - The score for each outcome, new game and opponent
#
# Output: log.txt     - Ongoing log of each scoring event
#         results.txt - The scores etc. for each player
#
#                                                     Joe Metcalfe    2013-03-03
#===============================================================================


def doWin():
    ''' One player has beaten another, do scoring with win flag set
    '''
    doScore(1)


def doDraw():
    ''' Two players have drawn, do scoring with win flag unset
    '''
    doScore(0)


def doScore(winDraw):
    ''' Score a game - generate appropriate text for winScoring & confirm
    '''
    global valW, valL, labScore, valWD
    # Winning and losing player (or drawn -- winDraw false)
    valW = wPlayer.get()
    valL = lPlayer.get()
    valWD = winDraw
    if (winDraw):
        t = " beat "
    else:
        t = " drew with "
    # Display appropriate string with players & game
    t = "\n" + str(players[valW]) + t + str(players[valL])
    t = t + " at " + str(games[selGame.get()]) + "\n"
    labScore.configure(text=t)
    labScore.grid(row=0, column=0, columnspan=2)
    winScoring.deiconify()


def doQuit():
    ''' End of program actions: write out scores, close window
    '''
    f = open("results.txt", "w")
    for p in range(nPlayer):
        f.write("{0} {1} {2}\n".format(p, playerScore[p][0], players[p]))
        f.write("{}\n".format(playerGames[p]))
        f.write("{}\n".format(playerOpps[p]))
    f.close()
    print "goodbye"
    root.withdraw()
    sys.exit()


def writeScore():
    ''' Update scores, write info to log & print, close scoring confirmation
    window
    '''
    valGame = selGame.get()
    # Update the winner and loser score etc.
    # When x is winner, y is loser and vice-cersa
    for x in [valW, valL]:
        y = valW + valL - x
        # Add the game if not present
        if (playerGames[x].count(valGame) == 0):
            playerGames[x].append(valGame)
            playerScore[x][0] = playerScore[x][0] + gameScore
        # Add the opponent if not present
        if (playerOpps[x].count(y) == 0):
            playerOpps[x].append(y)
            playerScore[x][0] = playerScore[x][0] + oppScore
        # Add the score for drawing, if appropriate
        if(not valWD):
            playerScore[x][0] = playerScore[x][0] + drawScore
            playerDrawn[x].append(valGame)
    # Add the score for winning and the game won
    if(valWD):
        playerScore[valW][0] = playerScore[valW][0] + winScore
        playerWon[x].append(valGame)

    # Add the game, winner, loser, won or drawn, and the time to the log
    f = open("log.txt", "a")
    x = "{0} {1} {2} {3} {4}\n".format(valGame, valW, valL, valWD, asctime())
    f.write(x)
    f.close()
    print x
    # Sort the scores, highest first and add the highest nLeader of them to
    # the leaderboard
    orderScore = sorted(playerScore, reverse=True)
    for jj in range(1, nLeader + 1):
        ii = orderScore[jj - 1][1]
        leaderText[jj][0].configure(text=players[ii])
        leaderText[jj][1].configure(text=len(playerGames[ii]))
        leaderText[jj][2].configure(text=len(playerOpps[ii]))
        leaderText[jj][3].configure(text=len(playerWon[ii]))
        leaderText[jj][4].configure(text=len(playerDrawn[ii]))
        leaderText[jj][5].configure(text=playerScore[ii][0])
    winScoring.withdraw()


def cancelScore():
    ''' Wrong score, cancel
    '''
    print "Cancelled"
    winScoring.withdraw()


def showLB():
    ''' Show the leaderboard
    '''
    leaderBoard.deiconify()
    print "Leader Board"


def cancelLB():
    ''' Hide the leaderboard
    '''
    leaderBoard.withdraw()


#-------------------------------------------------------------------------------
# Main program
#-------------------------------------------------------------------------------
from Tkinter import *
from time import asctime

# Start the main Tk window
root = Tk()
root.title("Tournament Scoring")
root.geometry("1300x650")

# Configure the first column; using "grid"
app = Frame(root)
app.grid()
app.columnconfigure(0, minsize=160)

# Fonts and titles
titleFont = ("Times", 30)
butFont = ("arial", 14)
norFont = ("arial", 12)
lbFont1 = ("arial", 20)
lbFont2 = ("arial", 26)
lbFont3 = ("Times", 40)

Label(app, text="Games", font=titleFont).grid(row=0, column=0, columnspan=2)
Label(app, text="Players", font=titleFont).grid(row=0, column=5, columnspan=2)

# Read the list of games, initialise radiobutton position,
# append game to 'games' & create a radiobutton for it
f = open("Games.txt", 'r')
x = 0
y = 1
yMax = 16
nGame = 0
selGame = IntVar()
games = []
for line in f:
    g = line.strip()
    games.append(g)
    rb = Radiobutton(app, text=g, variable=selGame, value=nGame,
                     font=norFont)
    rb.grid(row=y, column=x, sticky=W)
    # Update the radiobutton position, with new column if necessary
    y = y + 1
    if(y > yMax):
        y = 1
        x = x + 1
        app.columnconfigure(x, minsize=160)
    nGame = nGame + 1
f.close()

# Initialise lists of players, games they've played, opponents they've
# played against, their score, number of wins and losses
players = []
playerGames = []
playerOpps = []
playerScore = []
playerWon = []
playerDrawn = []
nPlayer = 0
wPlayer = IntVar()
lPlayer = IntVar()
# Add another coumnn to the GUI
x = x + 1
app.columnconfigure(x, minsize=160)
y = 1

# Read in the list of players, append each to 'players' and add a blank list for
# each to the above lists
f = open("Players.txt", 'r')
for line in f:
    p = line.strip()
    players.append(p)
    playerGames.append([])
    playerOpps.append([])
    playerWon.append([])
    playerDrawn.append([])
    playerScore.append([0, nPlayer])
    # Add a radiobutton  for each of the winning and losing players of each game
    # in two columns
    Label(app, text=p, font=norFont).grid(row=y, column=x, sticky=E)
    Radiobutton(app, text="W", variable=wPlayer, value=nPlayer,
                indicatoron=0, font=norFont).grid(row=y, column=x + 1)
    Radiobutton(app, text="L", variable=lPlayer, value=nPlayer,
                indicatoron=0, font=norFont).grid(row=y, column=x + 2)
    # Update the radiobutton position, adding three new columns if necessary
    y = y + 1
    if(y > yMax):
        y = 1
        x = x + 3
        app.columnconfigure(x, minsize=160)
    nPlayer = nPlayer + 1
f.close()

# Points for winning, drawing, playing a new opponent, a new game.
# The number players to be displayed on the leader board
f = open("scoring.txt", 'r')
winScore = int(f.readline().strip())
drawScore = int(f.readline().strip())
oppScore = int(f.readline().strip())
gameScore = int(f.readline().strip())
nLeader = int(f.readline().strip())
f.close()

# Set up 
y = yMax + 1
app.rowconfigure(y, minsize=80)

# Start the main application window and hide
winScoring = Toplevel(app)
winScoring.geometry("+200+200")
winScoring.withdraw()
labScore = Label(winScoring, text="Players", font=butFont)

# Start the leaderboard window and hide
leaderBoard = Toplevel(app)
leaderBoard.geometry("1000x400+200+200")
leaderBoard.columnconfigure(0, minsize=250)
leaderBoard.withdraw()
# Set up and apply the leaderboard title & column headings 
labLeader = Label(leaderBoard, text="Leader Board", font=lbFont3)
labLeader.grid(row=0, column=0, columnspan=6, sticky=EW)
leaderText = [[]]
lbTitles = ["Player ", "  Games", "  Opponents", "  Won", "  Drawn", "  Score"]
lbl = Label(leaderBoard, text=lbTitles[0], font=lbFont2)
lbl.grid(row=1, column=0, sticky=E)
# Add the leaderboard title and column headings to leaderText
leaderText[0].append(lbl)
for col in range(1, 6):
    lbl = Label(leaderBoard, text=lbTitles[col], font=lbFont2)
    lbl.grid(row=1, column=col)
    leaderText[0].append(lbl)

# Add '#' to blank cells of the leaderboard
for row in range(nLeader):
    leaderText.append([])
    lbl = Label(leaderBoard, text="#", font=lbFont1)
    lbl.grid(row=row + 2, column=0, sticky=E)
    leaderText[row + 1].append(lbl)
    for col in range(1, 6):
        lbl = Label(leaderBoard, text="#", font=lbFont1)
        lbl.grid(row=row + 2, column=col)
        leaderText[row + 1].append(lbl)

# Update a log file of all the scoring
f = open("log.txt", "a")
f.write("Start\n")
f.close()

# Create buttons to score a win or a draw, to display the leaderboard and
# to finish
Button(app, text="Win", command=doWin, font=butFont).grid(row=y, column=0,
                                                          sticky=S)
Button(app, text="Draw", command=doDraw, font=butFont).grid(row=y, column=1,
                                                            sticky=S)
bLB = Button(app, text="Leader Board", command=showLB, font=butFont)
bLB.grid(row=y, column=2, sticky=S)
Button(app, text="Exit", command=doQuit, font=butFont).grid(row=y, column=5,
                                                            sticky=S)
# Confirm or deny the entered score
bOK = Button(winScoring, text="OK", command=writeScore, font=butFont)
bOK.grid(row=1, column=0, sticky=S)
bCan = Button(winScoring, text="Cancel", command=cancelScore, font=butFont)
bCan.grid(row=1, column=1, sticky=S)

# leaderboard button to close itself
leaderBoard.rowconfigure(nLeader + 2, minsize=80)
bCanLB = Button(leaderBoard, text="Cancel", command=cancelLB, font=butFont)
bCanLB.grid(row=nLeader + 2, column=4, sticky=S)

# Start the GUI
root.mainloop()
